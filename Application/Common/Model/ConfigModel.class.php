<?php
namespace Common\Model;
use Think\Model;

class ConfigModel extends Model{

    protected $_validate = array(
        array('title','require','请填写配置名称',1,'regex',3),
        array('name','','该标识已存在',1,'unique',1),
        array('value','require','请填写配置值',1,'regex',3),
    );

    /**
     * 获取动态配置
     * @param string $module 模块的名称
     *
     */
    public function get_Config($module=''){
        if(empty($module)){
            $where = "status=1 AND module='Common'";
        }else{
            $where = "status=1 AND (module='Common' OR module='{$module}')";
        }
        $list = $this->field('name,value,type')->where($where)->select();
        $config = array();
        foreach ($list as $v) {
            $config[$v['name']] = $v['value'];
        }
        return $config;
    }


}

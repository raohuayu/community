<?php
namespace Common\Controller;
use Think\Controller;

class HomeBaseController extends Controller {

    /**
     * Home模块初始化
     */
    public function _initialize() {
        $this->load_Config();
    }

    /**
     * 动态加载配置
     */
    private function load_Config(){
        $home_config = F('HomeConfig');
        if(APP_DEBUG || !$home_config){
            $home_config = D('Common/Config')->get_Config('Home');
            F('HomeConfig',$home_config);
        }
        // 模板主题
        $home_config['TMPL_PARSE_STRING'] = array(
            '__THEME__' => __ROOT__.'/Public/Home/'.$home_config['DEFAULT_THEME'],
        );
        C($home_config);
    }

}




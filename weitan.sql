/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : weitan

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2016-11-24 13:32:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wt_doc
-- ----------------------------
DROP TABLE IF EXISTS `wt_doc`;
CREATE TABLE `wt_doc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `add_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `content` text NOT NULL COMMENT '内容',
  `is_del` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除,1是',
  PRIMARY KEY (`id`),
  KEY `add_time` (`add_time`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='文档数据表';

-- ----------------------------
-- Table structure for wt_doc_history_data
-- ----------------------------
DROP TABLE IF EXISTS `wt_doc_history_data`;
CREATE TABLE `wt_doc_history_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doc_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `update_time` int(10) unsigned DEFAULT '0' COMMENT '时间',
  `content` text COMMENT 'Markdown内容',
  `version` int(10) NOT NULL DEFAULT '1' COMMENT '当前文档的版本数，因为时间不固定，采用版本',
  PRIMARY KEY (`id`),
  KEY `doc_id` (`doc_id`),
  KEY `update_time` (`update_time`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='文档保存历史，版本控制';

-- ----------------------------
-- Table structure for wt_user
-- ----------------------------
DROP TABLE IF EXISTS `wt_user`;
CREATE TABLE `wt_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `salt` char(6) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态，1为正常，2为禁止',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除,1删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户表';

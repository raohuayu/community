项目开发指南
## 0.0.0.0002
- 增加ThinkPHP框架

## 0.0.0.0001

**环境配置**
- 环境：Windows7 + Apache2.4.23 + PHP 5.6 + mysql 5.7 
- 工具：PHPStorm 2016 
- CDN:http://www.bootcdn.cn/

**环境虚拟主机配置**
```
<VirtualHost *:80>
    ServerName t.com
    DocumentRoot H:/wamp64/www/community
    <Directory  "H:/wamp64/www/community">
        Options +Indexes +Includes +FollowSymLinks +MultiViews
        AllowOverride All
        Order allow,deny
            Allow from all
        DirectoryIndex index.php index.html
        #Require local
    </Directory>
</VirtualHost>
```

```
打开hosts文件
127.0.0.1 t.com
```




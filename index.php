<?php
define('APP_DEBUG',true);
define('BUILD_LITE_FILE',true);			        // 生成Lite文件
define('WEB_PATH', realpath('./').'/');         // 网站根路径
define('APP_PATH',WEB_PATH.'Application/');     // 项目路径
define('RUNTIME_PATH', WEB_PATH . 'Runtime/');  // 缓存目录
define('HTML_PATH', RUNTIME_PATH. 'Html/');     // 应用静态缓存目录
define('THINK_PATH', WEB_PATH. 'ThinkPHP/');    // 框架核心目录

// 自动生成模块
// define('BIND_MODULE','Admin');
// define('BUILD_CONTROLLER_LIST','Index,Public,Admin,Config,Data');

if(!APP_DEBUG && is_file(RUNTIME_PATH.'lite.php')) {
    require RUNTIME_PATH.'lite.php';    // 编译文件
} else {
    require THINK_PATH.'ThinkPHP.php';  // 核心文件
}